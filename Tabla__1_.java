public class Tabla
{
  public static void main (String[] args) {
    int n, i, potencia;
    n = 4;
    i = 1;
    System.out.println("\ta\ta^2\ta^3");
    while (i<=n) {
      System.out.print("\t");
      System.out.print(i);
      System.out.print("\t");
      potencia = (int)Math.pow(i,2);
      System.out.print(potencia);
      System.out.print("\t");
      potencia = (int)Math.pow(i,3);
      System.out.println(potencia);
      i = i + 1;
    }
  }

}
